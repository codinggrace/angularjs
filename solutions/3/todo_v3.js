angular.module('todoApp', [])
    .controller('ToDoCtrl', function($scope) {
         $scope.todo_items = [{'title': 'Build a todo app', 'done': false}] ;
		
		$scope.addToDo = function() {
			console.log("Calling add");
			$scope.todo_items.push({'title': $scope.newTodo, 'done': false})
			$scope.newTodo = '';	
		};
		
		$scope.removeToDo = function() {
			$scope.todo_items = $scope.todo_items.filter(function(item) {
				return !item.done;
			});
		};
		$scope.getTotalTodos = function() {
			return $scope.todo_items.length;
		}
 });