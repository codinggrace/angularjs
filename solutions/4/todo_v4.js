angular.module('todoApp', [])
    .controller('ToDoCtrl', ['$scope', '$http', function($scope, $http) {
        $scope.todo_items;
        $http.get('http://localhost:8000/todo.json').success(function(data) {
        		$scope.todo_items = data;
        });
		
		$scope.addToDo = function() {
			console.log("Calling add");
			$scope.todo_items.push({'title': $scope.newTodo, 'done': false})
			$scope.newTodo = '';	
		};
		
		$scope.removeToDo = function() {
			$scope.todo_items = $scope.todo_items.filter(function(item) {
				return !item.done;
			});
		};
 }]);